<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'App\Http\Controllers\HomeController@home');
Route::get('/cadastrar', 'App\Http\Controllers\VeiculoController@cadastrar');
Route::get('/excluir/{id}', 'App\Http\Controllers\VeiculoController@excluir');
Route::get('/alterar/{id}', 'App\Http\Controllers\VeiculoController@alterar');