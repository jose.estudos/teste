@extends('home') @section('content')
<form method="get" action="/cadastrar">
	<div class="form-row">
		<div class="col-7">
			<input type="text" name="nome" class="form-control" placeholder="Veiuclo"> <label
				for="inputState">Marca</label> <select id="inputState" name="marca"
				class="form-control">
				@foreach($veiculos as $veiculo)
				
				
				<option >{{$veiculo->marca}}</option>
				@endforeach
			</select>
		
		</div>
		  <button type="submit" class="btn btn-primary">cadastrar</button>

	</div>
</form>

<table class="table">
	<thead>
		<tr>
			<th scope="col">Marca</th>
			<th scope="col">Veiculo</th>
			<th scope="col">excluir</th>
		</tr>
	</thead>
	<tbody>
		@foreach($veiculos as $veiculo)
		<tr>
			<td>{{$veiculo->nome}}</td>
			<td>{{$veiculo->marca}}</td>
			<td><a href="/excluir/{{$veiculo->id}}">excluir</td>
		</tr>
		@endforeach
	</tbody>
</table>

@stop
