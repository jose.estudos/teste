<?php
namespace App\api;

use App\Models\Veiculos;

class VeiculosAPI
{
    function listarVeiculos() {
        return Veiculos::all();
    }
    
    function CadastrarVeiculo($marca,$nome) {
        $veiuclo = new Veiculos();
        $veiuclo->marca = $marca;
        $veiuclo->nome = $nome;
        $veiuclo->save();
        
    }
    function ExcluirVeiculo($id) {
        $veiuclo = Veiculos::find($id);
        $veiuclo->delete();
        
    }
    function AlterarVeiculo($id) {
        $veiuclo = Veiculos::find($id);
        
        return $veiuclo;
    }
}

