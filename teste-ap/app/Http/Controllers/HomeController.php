<?php

namespace App\Http\Controllers;

use App\api\VeiculosAPI;

class HomeController extends Controller
{
    function home() {
        $veiculos = new   VeiculosAPI();
        
        return view ('homeVeiculos')->with('veiculos',$veiculos->listarVeiculos());
    }
}
