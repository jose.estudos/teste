<?php

namespace App\Http\Controllers;

use App\api\VeiculosAPI;


class VeiculoController extends Controller
{
    function cadastrar() {
       $veiculo = new  VeiculosAPI();
       $marca= request('marca', "nao selecionado");
       $nome = request('nome', "nao selecionado");
       $veiculo->CadastrarVeiculo($marca, $nome);
       
       return redirect('/');
    }
    function alterar() {
        $veiculo = new  VeiculosAPI();
        $id =  request('id', "nao selecionado");
        $veiculo->AlterarVeiculo($id);
        
        return view ('homeVeiculos')->with('veiculo',$veiculo->AlterarVeiculo($id));
    }
    function excluir() {
        $veiculo = new  VeiculosAPI();
        $id =  request('id', "nao selecionado");
        $veiculo->ExcluirVeiculo($id);
        
        return redirect('/');
    }
}
